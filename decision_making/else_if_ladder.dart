//to check whether the num is positive or negative or zero
void main(){
  
  var num=2;
  if(num>0){
    print("Number ${num} is positive");
  }
  else if( num < 0){
    print("Number ${num} is negative");
  }
  else{
    print("Number ${num} is neither positive or negative");
  }
}