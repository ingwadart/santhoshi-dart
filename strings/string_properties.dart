//string interpolation or concatination

void main(){
  String str1="Hello";
  String str2="World";
  String res=str1 + str2;
  String Telugu ="తెలుగు";

  print("Concatenated string is ${str1 + str2}");

  //isEmpty : Returns true if this string is empty

  print("isEmpty :${str1.isEmpty}");

  print("isNotEmpty :${str1.isNotEmpty}");

  //length

  print("length of $str1 is ${str1.length}");

  //Returns a list of the UTF-16 code units of a given string.

  print("codeunits of $Telugu is ${Telugu.codeUnits}");

  //
}