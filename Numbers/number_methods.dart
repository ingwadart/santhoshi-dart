//number methods
void main(){
  var a=9.1;
  double n1=2.1;
  double n2=3.5;
  var value=10;

  int n3=3;

  var t= 2.3443333;

  //Absolute
  print("Absolute value of ${a} is ${a.abs()}"); 

   print("Ceil value of ${a} is ${a.ceil()} ");

   print("floor value of ${a} is ${a.floor()}");

  //comparision
  print("${a.compareTo(12)}");
  print("${a.compareTo(2)}");
  print("${a.compareTo(9.1)}");

  //round
  print("round of $n1 is ${n1.round()}");
  print("round of $n2 is ${n2.round()}");

  //convert double into  integer
  print("integer of $n1 is ${n1.toInt()}");

  //convert integer into double

  print("double of $n3 is ${n3.toDouble()}");

  //convert integer /double to string

  print("string of $n2 is ${n2.toString()}");

  //truncate : Returns an integer after discarding any fractional digits.  

  print("truncated value  of $t is ${t.truncate()}");

//remainder:It returns the truncated remainder after dividing the two numbers.

print("remainder:${value.remainder(2)}");


}