  //An enumeration is used for defining named constant values.
  enum status {
    Sun,
    Mon,
    Tue,
    Wed,
    Thu,
    Fri,
    Sat
  }

void main(){
  print("enum :${status.values}");

}