void main(){
  var details= new Map();
  details['firstname']='John';

  //addAll()
  details.addAll({'lastname':'Deo','no':'212'});
  print("details::${details}");

  //forEach()

  details.forEach((k,v)=> print('${k}: ${v}'));
}