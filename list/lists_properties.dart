void main() {
  var namesList = new List();
  namesList.add(1);
  namesList.add(2);
  namesList.add(3);
  print("names list :${namesList}");
  print("first element of list is :${namesList.first}");
  print("last element of the list is :${namesList.last}");
  print("isEmpty :${namesList.isEmpty}");
  print("isNotEmpty:${namesList.isNotEmpty}");
  print("Length:${namesList.length}");
  print("Reverse:${namesList.reversed}");
  print("single Element:${namesList.single}");
}
