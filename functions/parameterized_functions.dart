void main() {
  parameter(1, "This is String");
  optionalParameter("santhoshi");
  namedOptionalParameters(1,s1:'santhoshi');
  optionalParameterDefault(2,s1:'gffhhj',n2:2);
  arrowFunctions();
}

parameter(int n1, String s1) {
  print(" parameterized functions::$n1 and $s1");
}

optionalParameter(String name , [ int password] ) {

  print("optional parameter :: ${name} and ${password}");

}
namedOptionalParameters(int n1 , {String s1, String s2}){
  print("named parameters :: $n1 , $s1 and $s2");

}

optionalParameterDefault(int n1 ,{String s1 ,int n2:12}){
  print("optionalParameterDefault $n1");
  print("optionalParameterDefault $s1");
  print("optionalParameterDefault $n2");


}

arrowFunctions()=>print("This is Arrow Function or Lamda Function");
